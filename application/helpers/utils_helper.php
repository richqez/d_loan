<?php


function img_url(){
  return base_url() . 'assert/images/';
}

function formatDateToShow($source){

	$splitSource = explode("-", $source);
  $dd = $splitSource[2];
  $mm = $splitSource[1];
  $yyyy = $splitSource[0] + 543;
  return  $dd. '-' . $mm . '-' . $yyyy ;

}

function formatDateToSave($source){

	$splitSource = explode("-", $source);
  $dd = $splitSource[0];
  $mm = $splitSource[1];
  $yyyy = $splitSource[2] - 543;
  return  $yyyy. '-' . $mm . '-' . $dd ;

}


function thai_month($source){
  $splitSource = explode("-",$source);

  $dd = $splitSource[0];
  $mm = $splitSource[1];
  $yyyy = $splitSource[2] ;

  switch($splitSource[1]) {
    case "01":  $mm = "ม.ค."; break;
    case "02":  $mm = "ก.พ."; break;
    case "03":  $mm = "มี.ค."; break;
    case "04":  $mm = "เม.ย."; break;
    case "05":  $mm = "พ.ค."; break;
    case "06":  $mm = "มิ.ย."; break;
    case "07":  $mm = "ก.ค."; break;
    case "08":  $mm = "ส.ค."; break;
    case "09":  $mm = "ก.ย."; break;
    case "10":  $mm = "ต.ค."; break;
    case "11":  $mm = "พ.ย."; break;
    case "12":  $mm = "ธ.ค."; break;
  }
  return  $dd. '/' . $mm . '/' . $yyyy ;

}


 ?>
