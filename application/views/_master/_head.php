<!DOCTYPE HTML>
<html>

<head>
  <title>
    <?php if (isset($title_page)): ?>
      <?php echo $title_page ?>
        <?php else: ?>
          <?php echo "not set title page" ?>
            <?php endif; ?>

  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
  <script type="application/x-javascript">
    addEventListener("load", function() {
      setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
      window.scrollTo(0, 1);
    }
  </script>
  <link href="<?php echo base_url() . 'assert/css/' ?>bootstrap.min.css" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url() . 'assert/css/' ?>datatables.css" rel="stylesheet">
  <!-- Custom Theme files -->

  <link href="<?php echo base_url() . 'assert/css/' ?>style.css" rel='stylesheet' type='text/css' />
  <link href="<?php echo base_url() . 'assert/css/' ?>font-awesome.css" rel="stylesheet">
  <link href="<?php echo base_url() . 'assert/css/' ?>sweetalert.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url() . 'assert/css/' ?>select2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url() . 'assert/css/' ?>bootstrap-datepicker.css" rel="stylesheet" />

<style>
.center-text{
  text-align: center;
}
.table td, .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    color: #333;
}
.sidebar ul li a {
    color: #333;

}
.icon {
    float: none;
    margin-top: 0px;
}
.active-bg{
  background-color: #CCC
}
.dataTables_info{
  font-size: 13px
}
.pagination{
  font-size: 13px
}
</style>

<style media="screen">
  body{
    zoom:0.9;
  }
</style>


<script type="text/javascript" src="<?php echo base_url() . 'assert/js/' ?>jquery-2.2.0.js">

</script>
<script src="<?php echo base_url() . 'assert/js/' ?>datatables.js"></script>

  </script>
  <script src="<?php echo base_url() . 'assert/js/' ?>bootstrap.min.js">
  </script>

  <!-- Mainly scripts -->
  <script src="<?php echo base_url() . 'assert/js/' ?>jquery.metisMenu.js"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>jquery.slimscroll.min.js"></script>
  <!-- Custom and plugin javascript -->
  <link href="<?php echo base_url() . 'assert/css/' ?>custom.css" rel="stylesheet">
  <script src="<?php echo base_url() . 'assert/js/' ?>custom.js"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>screenfull.js"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>sweetalert.min.js"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>select2.min.js" charset="utf-8"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url() . 'assert/js/' ?>bootstrap-datepicker.th.js"></script>
  <script>
    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function() {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };


  </script>


  <script>
  		$(function () {
  			$('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

  			if (!screenfull.enabled) {
  				return false;
  			}



  			$('#toggle').click(function () {
  				screenfull.toggle($('#container')[0]);
  			});



  		});
  </script>



</head>

<body>
  <div id="wrapper">
    <!----->
    <?php include 'sidebar.php'; ?>
    <div id="page-wrapper" class="gray-bg dashbard-1">
      <div class="content-main">
