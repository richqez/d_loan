<div class="banner">
  <h2>
      <a href="index.html">จัดการบัญชีลูกค้า</a>
      <i class="fa fa-angle-right"></i>
      <span>ค้นหาและจัดการบัญชีลูกค้า</span>
      </h2>
</div>


<div class="blank">
  <div class="blank-page">
    <div class="row">
      <div class="col-md-2">
        <nav class="nav-sidebar" style="margin:0px">
          <div class="content-box">
            <ul id="customer_group_tab">
            <li><span>กรองตามสายงาน</span></li>
            <li list-item class="active-bg"><a href="#" data-rowid="all"><i class="fa fa-group nav_icon" ></i> ทุกสายงาน </a></li>
            <?php foreach ($group as $key => $value): ?>
              <li list-item><a href="#" data-rowid="<?php echo $value->customer_group_id ?>"><i class="fa fa-group nav_icon" ></i> <?php echo $value->customer_group_name ?> </a></li>
            <?php endforeach; ?>
            </ul>
          </div>

        </nav>
      </div>
      <div class="col-md-10">
        <div class="blank-page">


        <table id="account_table" class="table">
          <thead>
            <tr>
              <th>#</th>
              <th class="col-md-2">บัญชี</th>
              <th>
                สถาณะ
              </th>
              <th>ชื่อเจ้าของบัญชี,สายงาน</th>
              <!--th>เบอร์โทรศัพท์</th-->
              <!--th>สายงาน</th>
              <!--th>ดอกเบี้ย</th!-->
              <!--th>ประเภท</th!-->
              <th>ยอดกู้+ดอก</th>
              <th>ชำระแล้ว</th>
              <th>
                คงเหลือ
              </th>
              <th>

              </th>
              <th>

              </th>
              <th>

              </th>

            </tr>
          </thead>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="modelpayment">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">บันทึกการชำระเงิน</h4>
      </div>
      <div class="modal-body">

        <form id="payment_frm">
          <div class="form-group">
            <label >วันที่</label>
            <input type="text" class="form-control" id="date-payemnt" placeholder="วันที่" name="payment_date">
          </div>
          <div class="form-group">
            <label>จำนวนเงิน</label>
            <input type="text" class="form-control" placeholder="จำนวนเงิน" id="payment_amount" name="payment_amount">
          </div>
            <input type="hidden"  id="account_id" name="account_id">
        </form>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save_payment">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script type="text/javascript">

  var account_table = $('#account_table').DataTable({
    ajax       : '<?php echo base_url().'cus_account/datatable'?>',
    pageLength : 100,
    columnDefs : [
      {
        "targets"    : [0] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [2] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [3] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [4] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [5] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [6] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [7] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [8] ,
        "orderable"  : false,
        "searchable" : false
      },

    ],
    columns    : [
      {data : null },
      {data : 'account_name'},
      {
        mRender : function(data,type,full){

          var x = "";
          switch (full.account_status) {
            case '1':
              x = '<span class="label label-success">ลูกค้าชั้นดี</span>';
              break;
            case '2':
              x =  '<span class="label label-warning">เริ่มมีปัญหา</span>';
              break;
            case '3':
              x =  '<span class="label label-danger">หนี</span>';
              break;
            case '-1':
              x =  '<span class="label label-default">ปิดบัญชี</span>';
             break;
          }
          return x ;

        }
      },
      {data : 'customer_firstname'},
      {data : 'st'

      },
      {data : 'sum_payment_amount'},
      {
        data : 'balances'
      },
      {
        mRender : function(data,type,full){
          return '<a class="btn btn-success btn-sm" action-payment data-account_id ="'+full.account_id+'" data-account_name="'+full.account_name+'" >บันทึกการชำระเงิน</a>';
        },
        "sClass" : "center-text"
      },

      {
        mRender : function(data,type,full){
          return '<a class="btn btn-info btn-sm" target="_blank" href="'+'<?php echo base_url() ?>'+'cus_account/show_account_detail?account_id='+full.account_id+'" >ดูรายละเอียดทั้งหมด</a>';
        },
        "sClass" : "center-text"
      },


    ],
    language: {
        "url" : '<?php echo base_url() . 'assert/th.json' ?>'
      }
  });

   account_table.on( 'order.dt search.dt', function () {
     account_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
       cell.innerHTML = i+1;
     } );
   } ).draw();



  $('#account_table').on('click', 'a[action-payment]', function(event) {
    event.preventDefault();
    var account_id = $(this).data('account_id');
    var account_name = $(this).data('account_name');
    $('#account_id').val(account_id);
      $('#modelpayment').modal('toggle')
  });



</script>
<script type="text/javascript">
    var date_p  = $('#date-payemnt').datepicker({language:'th',format:'dd-mm-yyyy',todayHighlight :true,
     todayBtn:true,
     orientation:'bottom',
     autoclose:true
     });

  $('#save_payment').click(function(event) {


    if ($('#date-payemnt').val() != '' && $('#payment_amount').val() != ''  &&  !isNaN($('#payment_amount').val())  ) {
      var payment = $('#payment_frm').serializeObject();
      console.log(payment);

      $.post('<?php echo base_url() ?>cus_account/paid',payment)
        .done(function(data){
            if (data.result) {
              swal('ทำการบันทึกข้อมูลสำเร็จ','ทำรายการเรียบร้อย',"success");
               account_table.ajax.reload();
               $('#modelpayment').modal('toggle');
               $('#payment_amount').val('');
               $('#date-payemnt').val('');
            }else{
              swal('ไม่สามารถบันทึกยอด','มีการบันทึกยอดวันที่นี้ไปแล้ว',"error");
               account_table.ajax.reload();
               $('#modelpayment').modal('toggle');
               $('#payment_amount').val('');
               $('#date-payemnt').val('');
            }

        })
        .fail(function(data){
          swal('ไม่สำเร็จ','ระบบผิดพลาด',"error");
           account_table.ajax.reload();
            $('#modelpayment').modal('toggle');
            $('#payment_amount').val('');
            $('#date-payemnt').val('');
        })
    }else{
      swal('ไม่สำเร็จ','จำนวนเงิน ควรเป็นตัวเลข และ จำเป็นต้องเลือกวันที่',"error");
    }
  });

  $('#customer_group_tab').on('click','a',function(){
    $('li[list-item]').removeClass('active-bg');
    $(this).parent('li').addClass('active-bg');
    var group_id = $(this).data('rowid');
    if (group_id === 'all') {
      account_table.ajax.url('<?php echo base_url().'cus_account/datatable'?>').load();
    }else{
      account_table.ajax.url('<?php echo base_url().'cus_account/datatable'?>'+'?find_by_groupid='+group_id).load();
    }
  })
</script>
