<div class="banner">
  <h2>
      <a href="index.html">บัญชี</a>
      <i class="fa fa-angle-right"></i>
      <span>รายละเอียดการชำระเงิน</span>
      </h2>
</div>



<div class="blank">
   <div class="blank-page">
    <div class="row">
      <div class="col-md-12" id="control">
        <a class="btn btn-primary" target="_blank" href="<?= base_url() ?>Report/account_payment?account_id=<?php echo $info['account']->account_id ?>">พิมพ์ ประวติการชำระเงิน</a>

        <?php if ($info['account']->account_status != -1): ?>
          <a class="btn btn-success" href="<?= base_url() ?>cus_account/update_account_status?account_id=<?php echo $info['account']->account_id ?>&status=1" confrim>กำหนดเป็น ลูกค้าชั้นดี</a>
          <a class="btn btn-warning" href="<?= base_url() ?>cus_account/update_account_status?account_id=<?php echo $info['account']->account_id  ?>&status=2" confrim>กำหนดเป็น เริ่มมีปัญหา</a>
          <a class="btn btn-danger" href="<?= base_url() ?>cus_account/update_account_status?account_id=<?php echo $info['account']->account_id  ?>&status=3"  confrim>กำหนดเป็น หนี</a>


          <a class="btn btn-default btn-lg" style="float:right"  href="<?= base_url() ?>cus_account/update_account_status?account_id=<?php echo $info['account']->account_id  ?>&status=-1"  confrim>ปิดบัญชี</a>
          <a class="btn btn-default btn-lg" style="float:right" id="btn-delete-account" data-account_id="<?php echo $info['account']->account_id ?>">ลบบัญชี</a>
        <?php endif; ?>
      </div>
    </div>
  </div>


  <div class="blank-page">


    <div class="row" id="acount-content">


      <div class="col-md-3" >
        <div class="content-box">
          <ul id="customer_group_tab">
            <li><span>ข้อมูลลูกค้า</span></li>
            <div align="center" style="padding:10px">
              <img src="data:image/png;base64,<?php echo $info['customer']->customer_pic ?>" alt=""  class="img-thumbnail" style="    width: 300px;
  height: 200px;"/>
            </div>
            <table class="table">
                <tr>
                  <td>
                    <b>ชื่อลูกค้า</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_firstname ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>นามสกุล</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_lastname ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>สายงาน</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_group_name ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>ชื่อเล่น</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_nickname ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>ที่อยู่</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_address ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>เบอร์ติต่อ</b>
                  </td>
                  <td>
                    <?php echo $info['customer']->customer_tel ?>
                  </td>
                </tr>
            </table>
          </ul>
        </div>

      </div>
      <div class="col-md-6">

        <div class="content-box">
          <ul id="customer_group_tab">
            <li><span>รายการชำระเงิน</span></li>
            <table class="table">
              <thead>
                <tr>
                  <td class="col-md-3">
                    <b>จำนวนครั้งในการจ่ายทั้งหมด</b>
                  </td>
                  <td>
                    <?php echo sizeof($info['payments']) ?> ครั้ง
                  </td>
                </tr>
                <tr>
                  <td>
                    <b>ยอดรวมจ่ายทั้งหมด</b>
                  </td>
                  <td>
                    <?php if(!sizeof($info['payments']) == 0) {
                      echo $info['payments_sum'];
                    } ?>
                  </td>
                </tr>
                <tr>

                  <td>
                    <b>วันที่จ่ายล่าสุด</b>
                  </td>
                  <td>
                    <?php if(!sizeof($info['payments']) == 0) {
                      echo thai_month(formatDateToShow(date("Y-m-d",strtotime($info['payments'][0]->payment_date))));
                    }
                     ?>
                  </td>
                </tr>
              </thead>
            </table>

            <div style="padding:10px">
              <table class="table table-bordered" id="payment_table">
                <tr>
                  <th class="col-md-4">
                    วันที่จ่าย
                  </th>
                  <th class="col-md-3">
                    จำนวนเงิน
                  </th>
                  <th class="col-md-1">

                  </th>

                <?php foreach ($info['payments'] as $key => $value): ?>
                  <tr>

                    <td>
                     <?php echo thai_month(formatDateToShow(date("Y-m-d",strtotime($value->payment_date))))?>
                    </td>
                    <td>
                    <?php echo $value->payment_amount ?>
                    </td>
                    <td align="center">
                      <a class="btn btn-default" href="<?php echo base_url() ?>cus_account/delete_payment?account_id=<?php echo $info['account']->account_id ?>&payment_id=<?php echo  $value->payment_id ?>" role="button" delete_payment><span class="glyphicon glyphicon-remove" style="color:black" ></span></a>
                    </td>
                  </tr>
                <?php endforeach; ?>
              </table>

            </div>
          </ul>
        </div>

      </div>
      <div class="col-md-3">
        <div class="content-box">
          <ul id="customer_group_tab">
            <li><span>ข้อมูลบัญชี</span></li>
            <table  class="table">
              <tr>
                <td>
                  <b>ชื่อบัญชี</b>
                </td>
                <td>
                  <?php echo $info['account']->account_name ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>ยอดกู้</b>
                </td>
                <td>
                  <?php echo $info['account']->account_outstanding ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>ดอกเบี้ย</b>
                </td>
                <td>
                  <?php echo $info['account']->account_interest * 100 . ' %' ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>ยอดกู้+ดอกเบี้ย</b>
                </td>
                <td>
                  <?php echo $info['account']->account_outstanding + ( $info['account']->account_outstanding * $info['account']->account_interest  ) ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>ยอดคงเหลือ</b>
                </td>
                <td>
                  <?php

                        if ($info['account']->account_type != 2) {
                          echo $info['account']->account_outstanding + ( $info['account']->account_outstanding * $info['account']->account_interest  )  -   (($info['payments_sum']) ? $info['payments_sum'] : 0);
                        }else{
                          echo $info['account']->account_outstanding;
                        }
                          ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>ระยะเวลา</b>
                </td>
                <td>
                  <?php echo $info['account']->account_duration ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>จำนวนส่งต่อครั้ง</b>
                </td>
                <td>
                  <?php echo $info['account']->account_paypertimes ?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>วันที่กู้</b>
                </td>
                <td>
                  <?php echo thai_month(formatDateToShow($info['account']->account_startdate))?>
                </td>
              </tr>
              <tr>
                <td>
                  <b>สถาณะบัญชี</b>
                </td>
                <td>
                  <?php if ($info['account']->account_status == 1): ?>
                    <span class="label label-success">ลูกค้าชั้นดี</span>
                  <?php elseif($info['account']->account_status == 2): ?>
                    <span class="label label-warning">เริ่มมีปัญหา</span>
                  <?php elseif($info['account']->account_status == 3): ?>
                    <span class="label label-danger">หนี</span>
                  <?php elseif($info['account']->account_status == -1): ?>
                    <span class="label label-default">ปิดบัญชี</span>

                  <?php endif; ?>
                </td>

              </tr>
              <tr>
                <td>
                  <b>สิ่งรับประกัน</b>
                </td>
                <td>
                  <?php echo $info['account']->account_guarantee ?>
                </td>
              </tr>

            </table>
        </div>
      </div>

    </div>

  </div>
</div>
</div>
</div>

<script type="text/javascript">



  $('#control').on('click', 'a[confrim]', function(event) {
    var x = this;
    var y = $(this);
    event.preventDefault();
    swal({   title: "คุณต้องการ" +x.text+"  ?",
       text: "คุณจำเป็นต้องยิืนยัน",
       type: "warning",
       showCancelButton: true,
      confirmButtonColor: "#DD6B55",
       confirmButtonText: "ใช่ฉันต้องการ, ดำเนินการเลย",
       closeOnConfirm: false }, function(){
         window.location = x.href
       });
      });

    $('#payment_table').on('click','a[delete_payment]', function(event) {
      var link = this ;
      var date = $(this).parent().parent().children(':first').text();
      console.log($(this).parent().parent().children(':first'));
      var amount = $(this).parent().parent().children(':first').next().text();
      event.preventDefault();


      swal({   title: "คุณต้องการลบข้อมูลการชำระ\nวันที่" +$.trim(date)+"\nจำนวนเงิน"+$.trim(amount)+ " ?",
         text: "คุณจำเป็นต้องยิืนยัน",
         type: "warning",
         showCancelButton: true,
        confirmButtonColor: "#DD6B55",
         confirmButtonText: "ใช่ฉันต้องการ, ดำเนินการเลย",
         closeOnConfirm: false }, function(){
           window.location = link.href;
         });

    })


    $('#btn-delete-account').click(function(){
      var account_id = $(this).data('account_id');
      if (typeof account_id != 'undefined') {
        $.post('<?php echo base_url() ?>cus_account/delete_account',{account_id:account_id})
          .done(function(data){
            if (data.result) {
              sweetAlert("สำเร็จ...", "เรียบร้อย", "success");
              window.location = '<?php echo base_url() ?>cus_account/create_form';
            }else{
              sweetAlert("Oops...", "ไม่สามารถลบบัญชีได้เนื่องจากมีการบันทึกยอดไปแล้ว", "error");
            }
          })
          .fail(function(data){
            alert("ระบบเกิดข้อผิดพลาดโปรแจ้งผู้จัดทำ");
          })
      }else{
        alert("ระบบเกิดข้อผิดพลาดโปรแจ้งผู้จัดทำ");
      }

    })


</script>
