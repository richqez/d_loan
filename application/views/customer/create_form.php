<!--banner-->
<div class="banner">
  <h2>
      <a href="index.html">จัดการลูกค้า</a>
      <i class="fa fa-angle-right"></i>
      <span>สร้าข้อมูลลูกค้า</span>
      </h2>
</div>
<!--//banner-->
<!--faq-->
<div class="blank">

  <div class="grid-form">
    <div class="grid-form1">
      <h3 id="forms-example" class="">สร้างข้อมูลลูกค้า</h3>
      <?php if ($this->session->flashdata("result_message")!==NULL): ?>
        <div class="alert alert-success">
              <strong> <?php echo $this->session->flashdata("result_message") ?> !</strong>
          </div>
      <?php endif; ?>
      <?php if ($this->session->flashdata("result_message-error")!==NULL): ?>
        <div class="alert alert-danger" role="alert">
          <strong> <?php echo $this->session->flashdata("result_message-error") ?> !</strong>
        </div>
      <?php endif; ?>
      <form id="create_form" class="form-horizontal" action="<?php echo base_url() . 'cus/create' ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label hor-form">ชื่อ</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="inputEmail3" name="customer_firstname" placeholder="ชื่อ" required="" title="คุณจำเป็นต้องกรอก">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">นามสกุล</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="inputPassword3" name="customer_lastname" placeholder="นามสกุล" required="" title="คุณจำเป็นต้องกรอก">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">ชื่อเล่น</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="inputPassword3" name="customer_nickname" placeholder="ชื่อเล่น" required="" title="คุณจำเป็นต้องกรอก">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">ที่อยู่</label>
          <div class="col-sm-8">
            <textarea type="text" class="form-control" id="inputPassword3" name="customer_address" rows="4" placeholder="ที่อยู่" required="" title="คุณจำเป็นต้องกรอก"></textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">เบอร์โทรศัพท์</label>
          <div class="col-sm-8">
            <input id="tel" type="text" class="form-control" id="inputPassword3" name="customer_tel" placeholder="เบอร์โทรศัพท์" required=""  title="คุณจำเป็นต้องกรอก/ต้องเป็นตัวเลข">
          </div>
        </div>
         <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">สายงาน</label>
          <div class="col-sm-8">
            <select class="form-control" name="customer_group_id" id="customer_group_id">
                  <option value="null" >---เลือกสายงาน---</option>
                  <?php foreach ($group as  $value): ?>
                    <option value="<?php echo $value->customer_group_id;?> "><?php echo $value->customer_group_name; ?></option>
                  <?php endforeach; ?>
            </select>
          </div>
        </div>
         <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">รูป</label>
          <div class="col-sm-8">

              <input type="file" id="exampleInputFile" name="customer_pic">
              <p class="help-block"></p>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
              <button class="btn-primary btn" id="sub" type="submit">ตกลง</button>
              <button class="btn-inverse btn" type="reset">ล้าง</button>
          </div>
        </div>
      </form>
  </div>
  </div>

</div>
<script type="text/javascript">
  $('#create_form').submit(function(event){
    var  customer_group_id = $('#customer_group_id').val();
    if (customer_group_id === 'null') {
      event.preventDefault();
      swal("ไม่สำเร็จ!", "กรุณาเลือกกลุ่มลูกค้าก่อน...", "error");
    }else if (isNaN($('#tel').val()) || $('#tel').val() === ''){
      swal('ไม่สำเร็จ','เบอร์โทรศัพท์ ควรเป็นตัวเลข และ จำเป็นต้องกรอก',"error");
      event.preventDefault();
    }



  })
</script>
