<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends  CI_Model {

    private $table_name = "payments";

    public $payment_amount ;
    public $payment_date ;
    public $account_id ;


    public function __construct()
    {
    	parent::__construct();
    }

    public function paid(){

    	$this->payment_amount = $this->input->post('payment_amount');
    	$this->payment_date = formatDateToSave($this->input->post('payment_date'));
      $this->account_id = $this->input->post('account_id');

      $q = $this->db->get_where($this->table_name,['account_id'=>$this->account_id,'payment_date'=>$this->payment_date]);

      if ($q->num_rows() == 0) {
        $this->db->insert($this->table_name,$this);
        return true;
      }else{
        return false;
      }
    }

    public function find_by_accountid($account_id){
      $sql_command = "SELECT
                        *
                      FROM
                        payments
                      INNER JOIN
                        accounts ON accounts.account_id = payments.account_id
                      WHERE
                        accounts.account_id = ?
                      ORDER BY payments.payment_date ASC
                        ";

      $query = $this->db->query($sql_command,[$account_id]);
      return $query->result();

      }

    public function count_by_accountid($account_id){

      $q = $this->db->get_where($this->table_name,['account_id'=>$account_id]);

      return $q->num_rows();

    }

    public function delete_payment($payment_id){
      $this->db->delete($this->table_name,['payment_id'=>$payment_id]);
    }




}










 ?>
