<?php
/**
 * account : เบ้น
 */
class Account_model extends CI_Model
{

  private $table_name = "accounts";
  private $table_join_name = "customers";


  public $account_name;
  public $account_outstanding;
  public $account_interest;
  public $account_duration;
  public $account_guarantee;
  public $customer_id;
  public $account_startdate;
  public $account_type;
  public $account_status;
  public $account_paypertimes ;


  function __construct()
  {
    parent::__construct();
  }


  public function find_by_id($account_id){
    return $this->db->get_where($this->table_name,["account_id"=>$account_id])->row();
  }

  public function search(){

    if ($this->input->get('term') != null) {
      $sql_command = "SELECT * FROM customers INNER JOIN customer_groups ON customers.customer_group_id = customer_groups.customer_group_id WHERE customers.customer_firstname LIKE '%". $this->input->get('term') ."%'";

    }else{
      $sql_command ="SELECT * FROM customers INNER JOIN customer_groups ON customers.customer_group_id = customer_groups.customer_group_id LIMIT 20" ;
    }

    $q = $this->db->query($sql_command)->result();

    foreach ($q as $key => $value) {

      if ($q[$key]->customer_pic != null && $q[$key]->customer_pic != '') {
         $q[$key]->customer_pic = base64_encode($q[$key]->customer_pic);
      }


    }

    return $q;
  }



  public function create(){

    $count = $this->db->get_where($this->table_name,["customer_id"=>$this->input->post("customer_id")])->num_rows();
    $count += 1;


    $prifix = '';

    switch ($this->input->post("account_type")) {
      case '0':
        $prefix = $this->input->post("account_duration") . '-ด' ;
        break;
      case '1':
        $prefix = $this->input->post("account_duration") . '-ว' ;
        break;
      case '2':
        $prefix = $this->input->post("account_duration") . '-*^';
        break;
      default:
        $prefix = $this->input->post("account_duration") . '-ว' ;
        break;
    }



    $this->account_name = '('. $prefix .')'.  $this->input->post('customer_firstname') . '#' . $count;
    $this->account_outstanding = $this->input->post("account_outstanding");
    $this->account_interest = $this->input->post("account_interest") / 100;
    $this->account_duration = $this->input->post("account_duration");
    $this->account_guarantee = $this->input->post("account_guarantee");
    $this->customer_id = $this->input->post("customer_id");
    $this->account_startdate = formatDateToSave($this->input->post("account_startdate"));
    $this->account_type = $this->input->post("account_type");
    $this->account_status =  1 ;
    $this->account_paypertimes  = $this->input->post('account_payperday');

    $this->db->insert($this->table_name,$this);

    }



  public function find_all(){

    if ($this->input->get('find_by_groupid')!=NULL) {
      $sql_command = "SELECT account_id,account_status,account_name,customer_tel,customer_firstname,customer_group_name,IFNULL(SUM(sum_payment_amount),0) as sum_payment_amount,SUM(account_interest*100) AS account_interest ,account_outstanding ,CASE account_type WHEN 0 THEN 'รายเดือน'
      WHEN 1 THEN 'รายวัน' ELSE 'ดอกลอย'
      END AS account_type  ,
      IF(account_type = 2 ,account_outstanding + (account_outstanding*account_interest),COALESCE( (account_outstanding + (account_outstanding*account_interest)) - SUM(sum_payment_amount),(account_outstanding + (account_outstanding*account_interest))) ) as balances,
      account_outstanding + (account_outstanding*account_interest) as st

      FROM(
          SELECT customers.customer_group_id as customer_group_id,accounts.account_id as account_id,account_status as account_status ,account_name as account_name,account_type,customer_tel,customer_group_name,customer_firstname,payment_amount as sum_payment_amount,account_interest,account_outstanding
          FROM accounts
          LEFT JOIN payments ON payments.account_id = accounts.account_id
          INNER JOIN customers ON customers.customer_id = accounts.customer_id
          INNER JOIN customer_groups ON customer_groups.customer_group_id = customers.customer_group_id

      ) as payment_all WHERE customer_group_id = ? AND account_status != -1 AND account_status != 3 GROUP BY (account_id) ORDER BY account_name ASC ";


      $q = $this->db->query($sql_command,[$this->input->get('find_by_groupid')]);

    }else{
      $sql_command = "SELECT account_id,account_status,account_name,customer_tel,customer_firstname,customer_group_name,IFNULL(SUM(sum_payment_amount),0) as sum_payment_amount,SUM(account_interest*100) AS account_interest ,account_outstanding ,CASE account_type WHEN 0 THEN 'รายเดือน'
      WHEN 1 THEN 'รายวัน' ELSE 'ดอกลอย'
      END AS account_type ,
      IF(account_type = 2 ,account_outstanding + (account_outstanding*account_interest),COALESCE( (account_outstanding + (account_outstanding*account_interest)) - SUM(sum_payment_amount),(account_outstanding + (account_outstanding*account_interest))) ) as balances ,
      account_outstanding + (account_outstanding*account_interest) as st
      FROM(
          SELECT customers.customer_group_id as customer_group_id,accounts.account_id as account_id,account_status as account_status ,account_name as account_name,account_type,customer_tel,customer_group_name,customer_firstname,payment_amount as sum_payment_amount,account_interest,account_outstanding FROM accounts
          LEFT JOIN payments ON payments.account_id = accounts.account_id
          INNER JOIN customers ON customers.customer_id = accounts.customer_id
          INNER JOIN customer_groups ON customer_groups.customer_group_id = customers.customer_group_id

      ) as payment_all WHERE account_status != -1 GROUP BY (account_id) ORDER BY account_name ASC";

      $q = $this->db->query($sql_command);
    }


    return $q->result();

  }

  public function show_account_detail() {
    $sql_command = "SELECT * FROM accounts INNER JOIN customers ON customers.customer_id=accounts.customer_id INNER JOIN payments ON payments.account_id = accounts.account_id WHERE accounts.account_id =".+ $this->input->get('account_id') ;
    $q = $this->db->query($sql_command)->result();

    return $q;
  }



  public function update_account_status(){

    $account_id = $this->input->get('account_id');
    $account_status = $this->input->get('status');
    $this->db->update($this->table_name,["account_status" => $account_status],["account_id"=>$account_id]);

  }

  public function find_by_customer($customer_id){
    $sql_command = "SELECT * FROM accounts WHERE customer_id = ? ORDER BY account_startdate ASC";
    $q = $this->db->query($sql_command,[$customer_id])->result();
    return $q;
  }

  public function delete_account($account_id){

    $this->db->delete($this->table_name,['account_id'=>$account_id]);

  }





}



 ?>
